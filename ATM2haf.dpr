library ATM2haf;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  ExceptionLog,
  SysUtils,
  Classes,
  Atm in 'atm.pas',
  hanini in 'hanini.pas' {Frm_hanini},
  Dm_Atm in 'Dm_Atm.pas' {Frm_Data: TDataModule},
  epshavra in 'epshavra.pas' {Frm_epusHavra},
  DirSelct in 'DirSelct.pas' {Frm_DirSelect},
  //HanPrt in '\ATMSQL\HanPrt.pas' {Frm_Hanprt};
  HanPrt in 'HanPrt.pas' {Frm_Hanprt};
  {R *.RES$}
  exports
       InitAtmHan,
       DoneAtmHan,
       ShowAtmHan;

begin
  Frm_Main:=nil;
end.
