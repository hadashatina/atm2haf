unit Atm; {Delphi 5}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, Grids, StdCtrls, Buttons, DBGrids, Bde, DB,
  DBTables, {AdvSearch,} ToolEdit,inifiles
  {,
  ComCtrls, AtmSearch};

type
  TFrm_Main = class(TForm)
    From_sug: TLabel;
    Label1: TLabel;
    Tnua1: TEdit;
    Tnua2: TEdit;
    BitBtn1: TBitBtn;
    Dbg_Qryhsbon: TDBGrid;
    Bbn_Close: TBitBtn;
    From_tar: TLabel;
    To_tar: TLabel;
    Label2: TLabel;
    Shana: TEdit;
    Btn_Han: TBitBtn;
    tar1: TDateEdit;
    tar2: TDateEdit;
    Btn_epus: TBitBtn;
    Btn_doch: TButton;
    Button1: TButton;
    procedure BitBtn1Click(Sender: TObject);
    procedure Bbn_CloseClick(Sender: TObject);
    procedure Tar1Exit(Sender: TObject);
    procedure Tnua2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Tar2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure Btn_HanClick(Sender: TObject);
//  procedure SpdSapakClick(Sender: TObject);
    procedure Btn_epusClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Btn_dochMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure CopyFromFirstField(SecendField,FirstField : TEdit);
    { Public declarations }
  end;

  procedure MakeFileOfHan(TheUserName,ThePassword,TheAliasName :ShortString; ShnatMas : integer )
                         ; external 'AtmHaf.DLL';
  function DosToWin(WinString : String) : String;
  function TranslLetterDosToWin( Ch:char):char;
  function LeftToRight(InStr:string):string;
 {Atm Dll }
  procedure InitAtmHan(TheUserName,ThePassword,TheAliasName:PChar);
  procedure DoneAtmHan;
  procedure ShowAtmHan;

  {Meholel Dll}
  procedure OpenMeholel; external 'Meholel.dll';
  procedure CloseMeholel; external 'Meholel.dll';
  procedure ShowMain; external 'Meholel.dll';
  procedure LoadSavedDoh (Group,Shiyoh:Integer); external 'Meholel.dll';
  procedure ShowParamsScreen;external 'Meholel.dll';

var
  Frm_Main: TFrm_Main;
  MeholelOpened : Boolean;
  SapakF,SapakT : longint;
  Err1,Err2     : integer;
  ShnatMas      : integer;
  Hanprm        : Tinifile;
  Mavar4        : String;

implementation
USES Dm_Atm, Frm_Clnd, hanini, epshavra, DirSelct, HanPrt;
{$R *.DFM}

function LeftToRight(InStr:string):string;
 var
   ix1,i:integer;
   HelpStr,Tmp,Tmp1:string;
begin
  HelpStr:='';
  Tmp := '';
  Tmp1:='';
  for ix1:= Length(InStr) downto 1 do
     HelpStr:=HelpStr+InStr[ix1];
  for ix1:= 1 to Length(HelpStr) Do
  begin
      if ((HelpStr[ix1] <'0') Or (HelpStr[ix1] >'9'))And (HelpStr[ix1]<>'.')And (HelpStr[ix1]<>',')  And (HelpStr[ix1]<>'-') Then
      begin
         For i:= Length (tmp1)downto 1  do
            Tmp:=Tmp+Tmp1[i];
         Tmp1:='';
         Tmp:=Tmp+HelpStr[ix1];
      end
      else
        Tmp1 := Tmp1+HelpStr[ix1]
  end;
  For i:= Length (tmp1)downto 1  do
     Tmp:=Tmp+Tmp1[i];

  LeftToRight:=tmp;
end;
function TranslLetterDosToWin( Ch:char):char;
begin
   case ord(Ch) of
     40,41,46: TranslLetterDosToWin:=chr(32);
     127..156: TranslLetterDosToWin:=chr(ord(ch)+96);
     else TranslLetterDosToWin:=ch;
   end;
end;

function DosToWin(WinString : String) : String;
var
  CountI : Integer;
begin
  for CountI:=1 to Length(WinString) do
    WinString[CountI]:=TranslLetterDosToWin(WinString[CountI]);
  Result := WinString;
end;

procedure TFrm_Main.CopyFromFirstField(SecendField,FirstField : TEdit);
    Begin
         SecendField.Text:=FirstField.Text;
    End;


procedure TFrm_Main.BitBtn1Click(Sender: TObject);
var OK         : boolean;
Ind,mis,Ix1    : integer;
HStr           : shortstring;
Tstr           : TStringlist;
AliasDirectory : String;
PathExeFile : Shortstring; {Sql ����� �� ���� ����� -�� ������ ���� �}
ListParam : TStringList;    {Alias ����� �� �}
begin
  with Frm_Data do
  begin
//  Bitbtn1.Enabled:=false;
    Qry_Hsbon.Close;

    if Tnua1.Text = '' then
       Tnua1.Text := '0';
    if Tnua2.Text = '' then
       Tnua2.Text := '99999';
    if Tar1.text ='  /  /    ' Then
       Tar1.text :='01/01/1900';
    if Tar2.text ='  /  /    ' Then
       Tar2.text :='31/12/2999';
    If Shana.text = '' then
       Shana.text := (copy(datetostr(date),7,4));
       ShnatMas:=StrToint(Shana.text);

    P:=StrAlloc(100);
    GetWindowsDirectory(p,100);    {Windows �����}
    HanStr:=Strpas(p);
    StrDispose(p);
//  ============
//  Hanprm:=Tinifile.create(Hanstr+'\Atmhan.ini');
    Hanprm:=Tinifile.create(Hanstr+'\'+DBAliasName+'han.ini');
    Mikum:=Hanprm.readString('Han','Mikum',''); { ����� ���� ���"� }
    Mavar4:=Hanprm.readString('Han','Mavar4','');
    Hanprm.free;
    IF (Mikum='') Or (Mavar4='') Then
    Begin
      Showmessage('!! ����� ������ �� ����� ');
      Exit;
    End;

    val(Tnua1.Text,SapakF,Err1);
    OK:=(Err1 = 0);
    val(Tnua2.Text,SapakT,Err1);
    OK:=OK and (Err1 = 0);
    if OK then
      OK:=OK and (SapakF <= SapakT);
    if not(OK) then
      showmessage('�������� ���� ������')
    else
    begin   { parametres are good}

       Bitbtn1.Enabled:=false;
       with Frm_Data.Qry_Hsbon do
       begin
          ParamByName('PtnuF').Asinteger:= strtoint(Tnua1.Text);
          ParamByName('PtnuT').Asinteger:= strtoint(tnua2.Text);

          ParamByName('PdateF').AsDateTime:= Strtodate(Tar1.text);
          ParamByName('PdateT').AsDateTime:= strtoDate(Tar2.Text);
          ParamByName('PShnatmas').AsInteger:= strtoInt(Shana.text);

          Screen.Cursor:=crHourGlass;
        try
          Prepare;
          Tbl_Hashboniot.DatabaseName:=ShemAlias;          
          IF Tbl_Hashboniot.Exists Then Tbl_Hashboniot.DeleteTable;
          Frm_Data.Qry_Hsbon.Open;
          Tbl_Hashboniot.BatchMove(Frm_Data.Qry_Hsbon,batCopy);
          Qry_hsbon.Close;
          Tbl_Hashboniot.AddIndex('LakHash','MisparHafkada;MisparShura',[ixPrimary{,ixUnique}]);
       Except
         On e:exception do
         Begin
           Showmessage('���� ������� �������   '+#13+'�� ����� �� ������ ������ ���');
           Screen.Cursor:=CrDefault;
           Exit;
         End;
         End;
       end;
       Screen.Cursor:=crDefault;
       Application.ProcessMessages;
       Tbl_Hashboniot.Open;
       Dbg_Qryhsbon.DataSource:=DS_Hashboniot;
       Dbg_Qryhsbon.Refresh;
       Screen.Cursor:=crDefault;
//     Tbl_Hashboniot.Close;
       MakeFileOfHan(AtmUserName,AtmPassword,DBAliasName,ShnatMas);
    end; { parametres are good}
  END; { with Frm_Data do }

end;

procedure TFrm_Main.Bbn_CloseClick(Sender: TObject);

begin
  with Frm_Data do
  begin
    Tbl_Hashboniot.Close;
    MyDataBase.Close;
  end;


end;

procedure TFrm_Main.Tar1Exit(Sender: TObject);
var
   TmpDate : tdatetime;
begin
    if (Sender as tmaskedit).text <> '  /  /    ' Then
    begin
    try
      TmpDate := StrToDate ((Sender as tmaskedit).text);
    except
       ON Exception do
        begin
         Showmessage('!!! ����� ���� ��� ���� ');
         (Sender as tmaskedit).text :='  /  /    ';
         (Sender as tmaskedit).setfocus;
        end;
    end;
    End;
end;


procedure TFrm_Main.Tnua2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
         Vk_f9       :CopyFromFirstField(tnua2,tnua1);
       End;
end;


procedure TFrm_Main.Tar2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
         Vk_f9       :Tar2.text:=Tar1.text;
    End;

end;


procedure TFrm_Main.FormCreate(Sender: TObject);
begin
  MeholelOpened := False;
  Caption:=Caption+'   '+ 'Alias='+ DBAliasName ;  
//Frm_hanini:=Nil;
//Frm_epusHavra:=Nil;
end;

procedure TFrm_Main.Btn_HanClick(Sender: TObject);
begin
   if Frm_hanini = nil then
     Frm_hanini:=TFrm_hanini.Create(Nil);
   Frm_hanini.showModal;
// Frm_hanini.Free;
// Frm_hanini:=Nil;
end;


procedure TFrm_Main.Btn_epusClick(Sender: TObject);
begin
   if Frm_epusHavra = nil then
      Frm_epusHavra:=TFrm_epusHavra.Create(Nil);
   Frm_epusHavra.showModal;
// Frm_epusHavra.Free;
// Frm_epusHavra:=Nil;

end;

procedure TFrm_Main.FormDestroy(Sender: TObject);
begin
  if MeholelOpened then
      CloseMeholel;
  MeholelOpened := False;
end;

procedure TFrm_Main.Btn_dochMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
{
     if Button in [mbLeft] then
     begin
         If Not MeholelOpened Then
            OpenMeholel;
         MeholelOpened := True;
         LoadSavedDoh(-1,-1);
         ShowParamsScreen;
     end
     else
        If (Button in [mbRight]) And (ssAlt in Shift ) Then
        begin
         If Not MeholelOpened Then
            OpenMeholel;
         MeholelOpened := True;
         ShowMain;
     end
}
end;

procedure InitAtmHan(TheUserName,ThePassword,TheAliasName:PChar);
Var
    P :Pchar ;
    TmpStr,TmpStr1,Line  : String;
    FileHan,Newhan       : TextFile;
begin
   AtmUserName:=StrPas(TheUserName);
   AtmPassword:=StrPas(ThePassword);
   DBAliasName:=StrPas(TheAliasName);

   AtmUserName:=StrPas(TheUserName);
   AtmPassword:=StrPas(ThePassword);
   DBAliasName:=StrPas(TheAliasName);
    P:=StrAlloc(100);
    GetWindowsDirectory(p,100);    {Windows �����}
    TmpStr:=Strpas(P);
    StrDispose(p);
    TmpStr1:=TmpStr+'\'+DBAliasName+'han.ini';
   If FileExists(TmpStr+'\Atmhan.ini') Then
   Begin
     If Not FileExists(TmpStr1) Then
     Begin
       AssignFile(FileHan,TmpStr+'\Atmhan.ini');
       Reset(FileHan);
       AssignFile(Newhan,TmpStr1);
       Rewrite(NewHan);
       While not Eof(FileHan) Do
       Begin
         Readln (FileHan,Line);
         Writeln(NewHan,Line);
       End; //While not Eof
       CloseFile(FileHan);
       CloseFile(NewHan);
     End; //not FileExists(TmpStr1)
   End; //FileExists(TmpStr+'\Atmhan.ini')


   if Frm_Main = Nil Then
   begin
     Frm_Main := TFrm_Main.Create (Nil);
     Frm_hanini := TFrm_hanini.Create (Nil);
     Frm_Data := TFrm_Data.Create (Nil);
     Frm_epusHavra := TFrm_epusHavra.Create (Nil);
     Frm_DirSelect := TFrm_DirSelect.Create (Nil);
   end;
end;

procedure DoneAtmHan;
begin
   try
     Session.Databases[0].Connected := false;
   except
     ON Exception do
   End;

   If Frm_Main <> nil then
   begin
        If Frm_DirSelect<>Nil Then
        Begin
          Frm_DirSelect.Free;
          Frm_DirSelect := Nil;
        End;
        if Frm_epusHavra <> Nil then
        begin
          Frm_epusHavra.Free;
          Frm_epusHavra := Nil;
        End;
        Frm_Data.Free;
        Frm_Data := Nil;
        if Frm_hanini <> Nil then
        Begin
          Frm_hanini.Free;
          Frm_hanini := Nil;
        End;
        If Frm_HanPrt <> Nil Then
        Begin
          Frm_HanPrt.Free;
          Frm_HanPrt := Nil;
        End;
        
        Frm_Main.Free;
        Frm_Main := Nil;
   end;
end;

procedure ShowAtmHan;
begin
     Frm_Main.ShowModal;
end;

procedure TFrm_Main.Button1Click(Sender: TObject);
Var
Hadfasa  : String;
begin
     if Frm_Hanprt = Nil then
        Frm_HanPrt:=TFrm_HanPrt.Create(Nil);
     Hadfasa:='�';
     If Not(InputQuery(' ��� ������', '���/� [�] ������ �� ��� ��� ����' ,Hadfasa)) Then
       Exit;

     If Hadfasa<>'�' Then
       Frm_HanPrt.QuickRep1.Preview
     Else
       Frm_HanPrt.QuickRep1.Print;

end;

end.

