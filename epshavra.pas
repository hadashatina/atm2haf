unit epshavra;

interface

uses Windows, Dialogs, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, DBTables,inifiles;

type
  TFrm_epusHavra= class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    Label1: TLabel;
    Edt_frompkoda: TEdit;
    Label2: TLabel;
    Edt_Topkoda: TEdit;
    Label3: TLabel;
    Edt_ShnatMas: TEdit;
    procedure OKBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_epusHavra : TFrm_epusHavra;

implementation

uses Dm_Atm,Hanini;

{$R *.DFM}

procedure TFrm_epusHavra.OKBtnClick(Sender: TObject);

var
Ix1,mis        : integer;
HStr,mikum     : shortstring;
Hanprm         : Tinifile;
//AliasDirectory : String;
//ListParam      : TStringList;    {Alias ����� �� �}
P              :Pchar ;

begin
    If Edt_ShnatMas.text = '' then
        Edt_ShnatMas.text := (copy(datetostr(date),7,4));

    If (Edt_frompkoda.text='') Or
        (Edt_Topkoda.text='')  Or
        (Edt_frompkoda.text>Edt_Topkoda.text)  then
         showmessage('�������� ���� ������')
    Else
    Begin
       P:=StrAlloc(100);
       GetWindowsDirectory(p,100);
       HStr:=Strpas(p);
       StrDispose(p);
//     Hanprm:=Tinifile.create(Hstr+'\Atmhan.ini');
       Hanprm:=Tinifile.create(Hstr+'\'+DBAliasName+'han.ini');
       Mikum:=Hanprm.readString('Han','Mikum',''); { ����� ���� ���"� }

       with Frm_Data.Qry_EpusHes do
       begin
          ParamByName('PpkodaF').AsInteger:= strtoint(Edt_Frompkoda.text);
          ParamByName('PpkodaT').AsInteger:= strtoint(Edt_Topkoda.text);
          ParamByName('Pshnatmas').AsInteger:= strtoint(Edt_ShnatMas.text);
          Screen.Cursor:=crHourGlass;
          Prepare;
          ExecSql;
          Screen.Cursor:=crDefault;
          Edt_frompkoda.text:='';
          Edt_Topkoda.text:='';
          showmessage('������ ������');
          If FileExists(mikum+'\MoveIn.Dat') Then DeleteFile(mikum+'\MoveIn.Dat');
       end;
    End; {��������  ������}
end;

end.
