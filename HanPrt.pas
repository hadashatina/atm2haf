unit HanPrt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls ,inifiles;

type
  TFrm_Hanprt = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRBand2: TQRBand;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRBand4: TQRBand;
    Qlb_Hova: TQRLabel;
    Qlb_Zchut: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    Qlb_Asm1: TQRLabel;
    Qlb_date1: TQRLabel;
    QRShape9: TQRShape;
    Qlb_Asm2: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    Qlb_Date2: TQRLabel;
    QRShape14: TQRShape;
    Qlb_detail: TQRLabel;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    Qlb_Sum: TQRLabel;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRBand5: TQRBand;
    QRLabel10: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel11: TQRLabel;
    QRSysData2: TQRSysData;
    QRBand3: TQRBand;
    QRShape19: TQRShape;
    Qlb_Sign: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    Qlb_THova: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape2: TQRShape;
    QRShape20: TQRShape;
    QRLabel15: TQRLabel;
    Qlb_Dollar: TQRLabel;
    QRLabel16: TQRLabel;
    QRMemo_CompanyDetails: TQRLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRep1AfterPreview(Sender: TObject);
    procedure QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRep1AfterPrint(Sender: TObject);
    procedure Qlb_SumPrint(sender: TObject; var Value: String);
    procedure QRLabel14Print(sender: TObject; var Value: String);
    procedure Qlb_THovaPrint(sender: TObject; var Value: String);
    procedure FormCreate(Sender: TObject);
    procedure QRMemo_CompanyDetailsPrint(sender: TObject;
      var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function DoEncryption(Src:string; Key: string; DecryptKey: integer; Encrypt: Boolean):string;

var
    Frm_Hanprt: TFrm_Hanprt;
    Hanprm                           : Tinifile;
    Hanstr,Mikum,MisPkoda            : ShortString;
    line                             : Shortstring;
    FileHan                          : TextFile;
    P                                : Pchar ;
Const //MNAME �����
     Mname_Key  = 'Atm2000Mname';
     MnameDecryptKey = 182;

implementation

uses Atm, Dm_Atm ,Hanini;
var
    Hova,Zchut,Sum                   : Real;

{$R *.DFM}

function DoEncryption(Src:string; Key: string; DecryptKey: integer; Encrypt: Boolean):string;
var
   KeyLen      :Integer;
   KeyPos      :Integer;
   offset      :Integer;
   dest        :string;
   SrcPos      :Integer;
   SrcAsc      :Integer;
   TmpSrcAsc   :Integer;

begin
  Try
     Result:='';
     if Src = '' Then
        Exit;
     KeyLen:=Length(Key);
     if KeyLen = 0 then Key:='Aviran';
     KeyPos:=0;
     SrcPos:=0;
     SrcAsc:=0;
     if Encrypt then begin
          offset:=DecryptKey;
          dest:=format('%1.2x',[offset]);
          for SrcPos := 1 to Length(Src) do
          begin
               SrcAsc:=(Ord(Src[SrcPos]) + offset) MOD 255;
               if KeyPos < KeyLen then KeyPos:= KeyPos + 1 else KeyPos:=1;
               SrcAsc:= SrcAsc xor Ord(Key[KeyPos]);
               dest:=dest + format('%1.2x',[SrcAsc]);
               offset:=SrcAsc;
          end;
     end
     else
     begin
          offset:=StrToInt('$'+ copy(src,1,2));
          SrcPos:=3;
          repeat
                SrcAsc:=StrToInt('$'+ copy(src,SrcPos,2));
                if KeyPos < KeyLen Then KeyPos := KeyPos + 1 else KeyPos := 1;
                TmpSrcAsc := SrcAsc xor Ord(Key[KeyPos]);
                if TmpSrcAsc <= offset then
                     TmpSrcAsc := 255 + TmpSrcAsc - offset
                else
                     TmpSrcAsc := TmpSrcAsc - offset;
                dest := dest + chr(TmpSrcAsc);
                offset:=srcAsc;
                SrcPos:=SrcPos + 2;
          until SrcPos >= Length(Src);
     end;
     Result:=Dest;
  Except 
//         ShowMessage('����� ������');
  End;
end;


procedure TFrm_Hanprt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//      Frm_HanPrt.Free;
//      Frm_HanPrt := Nil;
end;

procedure TFrm_Hanprt.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
     Hova:=0;
     Zchut:=0;
     P:=StrAlloc(100);
     GetWindowsDirectory(p,100);
     HanStr:=Strpas(p);
     StrDispose(p);
//   Hanprm:=Tinifile.create(Hanstr+'\Atmhan.ini');
     Hanprm:=Tinifile.create(Hanstr+'\'+DBAliasName+'han.ini');
     Mikum:=Hanprm.readString('Han','Mikum',''); { ����� ���� ���"� }
     Mispkoda:=Hanprm.readString('Han','Mispkoda','0');
     QRLabel16.Caption:=Mispkoda;
     AssignFile(FileHan,mikum+'\MoveIn.Dat');
     Reset (FileHan);
     Readln (FileHan,Line);
end;

procedure TFrm_Hanprt.QuickRep1AfterPreview(Sender: TObject);
begin
   Try
     CloseFile (FileHan);
   Except
   end;
end;

procedure TFrm_Hanprt.QuickRep1NeedData(Sender: TObject;
  var MoreData: Boolean);

var
    Str88 : array[0..87] of char ;

begin
 { Fillchar(str88, sizeof(str88), '9');
  if Not eof (FileHan) then
     begin
       Readln (FileHan,Line);
       If (Copy(Line,1,88)=Str88) Then
       Begin
          MoreData :=False;
          CloseFile (FileHan);
       End
       Else
       Begin   }
        if Not eof (FileHan) then
     begin
         MoreData :=True;
         Qlb_hova.Caption  := LeftToRight(DosToWin(Copy(Line,102,15)));
         Qlb_Zchut.Caption := LeftToRight(DosToWin(Copy(Line,132,15)));
         If Qlb_hova.Caption=Qlb_Zchut.Caption Then
         begin
           Qlb_Zchut.Caption:='********';
           Qlb_hova.Caption:='********';
         End;
         Qlb_Asm1.Caption  := Copy(Line,4,9);
         Qlb_Date1.Caption := Copy(Line,22,2)+'/'+Copy(Line,25,2)+'/'+Copy(Line,30,2);
         Qlb_Asm2.Caption  := Copy(Line,13,9);
         Qlb_Date2.Caption := Copy(Line,32,2)+'/'+Copy(Line,35,2)+'/'+Copy(Line,40,2);
         Qlb_Detail.Caption:= LeftToRight(DosToWin(Copy(Line,52,50)));
         Qlb_sign.Caption:=Copy(Line,162,1);
         Qlb_sum.Caption   := Copy(Line,163,8)+'.'+Copy(Line,171,2);
         Qlb_Dollar.Caption:= Copy(Line,207,8)+'.'+Copy(Line,215,2);
         Sum:=Round(StrToFloat(Copy(Line,163,11)));
         if Copy(Line,162,1)='-' Then Sum:=Sum*-1;

         If (Qlb_hova.Caption<>'********') And
            (Qlb_hova.Caption<>'        ')
         Then
           Hova:=Hova+Sum
         Else
           If (Qlb_Zchut.Caption<>'********') And
              (Qlb_Zchut.Caption<>'        ')
         Then
             Zchut:=Zchut+Sum;
       End


  else
     begin
       MoreData := False;
       CloseFile (FileHan);
     end;
end;

procedure TFrm_Hanprt.QuickRep1AfterPrint(Sender: TObject);
begin
// Try
//   CloseFile (FileHan);
// Except
// end;
 end;

procedure TFrm_Hanprt.Qlb_SumPrint(sender: TObject; var Value: String);
begin
    try
//    TotalFromcheck := TotalFromcheck + StrToFloat(Value);
//    Value := FormatFloat ('###,###,##0.00',StrToFloat(Value)/100);
    except
    end;
end;

procedure TFrm_Hanprt.QRLabel14Print(sender: TObject; var Value: String);
begin
      Value := FormatFloat ('###,###,##0.00',(Zchut)/100);
end;

procedure TFrm_Hanprt.Qlb_THovaPrint(sender: TObject; var Value: String);
begin
      Value := FormatFloat ('###,###,##0.00',(Hova)/100);

end;

procedure TFrm_Hanprt.FormCreate(Sender: TObject);
begin
   Frm_Data.Qry_Mname.open;

end;

procedure TFrm_Hanprt.QRMemo_CompanyDetailsPrint(sender: TObject;
  var Value: String);
begin
  Value :=DoEncryption(Frm_Data.Qry_Mname.FieldByName('CompanyDetail').AsString,Mname_Key,MnameDecryptKey,False);
end;

end.
