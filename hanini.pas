unit hanini;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Dialogs, Grids,
  Outline, DirOutln ,inifiles, FileCtrl;

type
  TFrm_hanini = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Mikum_han: TEdit;
    Mis_pkoda: TEdit;
    Spb_buttonMikum: TSpeedButton;
    Label9: TLabel;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure Spb_buttonMikumClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frm_hanini: TFrm_hanini;
    P :Pchar ;
    HanStr,mikum,MisMaam,MisPkoda          : shortstring;
    MisMaam1,MisPkoda1                     : shortstring;
    MisMaam2,MisPkoda2                     : shortstring;
    MisMaam3,MisPkoda3                     : shortstring;
    DirectoryIniFiles                      : shortstring;
    Hanprm,Nzrprm                          : Tinifile;
    PerutMaam,Asmachta2,HesbonNegdi,
    Tnuatzikuy,RikuzMiun,PratimLeHan       : integer;
    MisHevra,MisHevra1,MisHevra2,MisHevra3 :shortstring ;
    AtmUserName,
    AtmPassword,
    DBAliasName   : String;

implementation

uses DirSelct;

{$R *.DFM}

procedure TFrm_hanini.FormCreate(Sender: TObject);
begin
    Frm_DirSelect := nil;
    P:=StrAlloc(100);
    GetWindowsDirectory(p,100);    {Windows �����}
    HanStr:=Strpas(p);
    StrDispose(p);
//  ============
//  Hanprm:=Tinifile.create(Hanstr+'\Atmhan.ini');
    Hanprm:=Tinifile.create(Hanstr+'\'+DBAliasName+'han.ini');
    Mikum:=Hanprm.readString('Han','Mikum',''); { ����� ���� ���"� }
    {Ini  ����� ���� }

    Mispkoda:=Hanprm.readString('Han','Mispkoda','0');

    Mikum_han.text:=Mikum;
    Mis_Pkoda.text:=Mispkoda;
    Hanprm.free;
end;

procedure TFrm_hanini.OKBtnClick(Sender: TObject);
begin
//      Hanprm:=Tinifile.create(Hanstr+'\Atmhan.ini');
        Hanprm:=Tinifile.create(Hanstr+'\'+DBAliasName+'han.ini');

        Hanprm.WriteString('Han','Mikum',Mikum_han.text);
        Hanprm.WriteString('Han','MisPkoda',Mis_Pkoda.text);
        Hanprm.free;
end;

procedure TFrm_hanini.Spb_buttonMikumClick(Sender: TObject);
Var
  Slash :String[1];
begin
     if Frm_DirSelect = nil then
        Frm_DirSelect:=Tfrm_DirSelect.Create(Application);
     If Frm_DirSelect.ShowModal = mrOk Then
     begin
       Slash := Copy (Frm_DirSelect.DirectoryListBox1.Directory,Length(Frm_DirSelect.DirectoryListBox1.Directory),1);
       if Slash = '\' then
          Mikum_han.Text:=Frm_DirSelect.DirectoryListBox1.Directory
       else
          Mikum_han.Text:=Frm_DirSelect.DirectoryListBox1.Directory+'\';
     end;
     Frm_DirSelect.Free;
     Frm_DirSelect := nil;
end;

end.
